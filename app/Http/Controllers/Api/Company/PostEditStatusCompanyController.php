<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyEditor;
use App\Http\Requests\Company\EditStatusCompanyRequest;

class PostEditStatusCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\EditStatusCompanyRequest $request
     */
    public function __invoke(EditStatusCompanyRequest $request, CompanyEditor $service)
    {
        
        DB::beginTransaction();
        try {
            $company = $service->handle($request->id);
            DB::commit();
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
