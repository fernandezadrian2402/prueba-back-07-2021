<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyList;

class GetListCompaniesController extends Controller
{
    /**
     * Create new company
     *
     *
     */
    public function __invoke(CompanyList $service)
    {
        DB::beginTransaction();
        try {
            $companies = $service->handle();
            DB::commit();
            return response($companies, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
