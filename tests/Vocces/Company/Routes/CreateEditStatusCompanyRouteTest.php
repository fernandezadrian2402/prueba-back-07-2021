<?php

namespace Tests\Vocces\Company\Routes;

use Tests\TestCase;
use Illuminate\Support\Str;
use App\Models\Company;

class CreateEditStatusCompanyRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function postEditStatusCompanyRoute()
    {
    
        $company = Company::all()->random(1)->first();
        /**
         * Preparing
         */
        $faker = \Faker\Factory::create();
        $testCompany = [
            'id'   => $company->id,
            'name'   => '',
            'email'   => '',
            'address'   => '',
            'status' => 'active',
        ];

        /**
         * Actions
         */
        $response = $this->json('POST', '/api/edit-status-company', [
            'id' => $testCompany['id'],
        ]);

        /**
         * Asserts
         */
        $response->assertStatus(201)
            ->assertJsonFragment($testCompany);
    }
}
