<?php


namespace Test\Vocces\Company\Application;

use Tests\TestCase;
use Illuminate\Support\Str;
use Vocces\Company\Domain\Company;
use Vocces\Company\Application\CompanyEditor;
use Tests\Vocces\Company\Infrastructure\CompanyRepositoryFake;

class EditStatusCompanyTest extends TestCase
{
    /**
     * @group application
     * @group company
     * @test
     */
    public function EditStatusCompany()
    {
        /**
         * Preparing
         */
        $faker = \Faker\Factory::create();
        $testEditCompany = [
            'id'     => '3b8204f5-127a-4397-b735-277d0738ced8',
            'name'   =>  '',
            'email'   => '',
            'address'   => '',
            'status' => 'active',
        ];

        /**
         * Actions
         */
        $editor = new CompanyEditor(new CompanyRepositoryFake());
        $company = $editor->handle(
            $testEditCompany['id']
        );

        /**
         * Assert
         */
        $this->assertInstanceOf(Company::class, $company);
        $this->assertEquals($testEditCompany, $company->toArray());
    }
}
