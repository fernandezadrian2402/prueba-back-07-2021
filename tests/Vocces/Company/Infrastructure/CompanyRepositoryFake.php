<?php

namespace Tests\Vocces\Company\Infrastructure;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodCreate = false;
    public bool $callMethodEditStatus = false;
    public bool $callMethodGetList = false;
    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
    }
    public function editStatus(Company $company): void
    {
        $this->callMethodEditStatus = true;
    }
    public function getList()
    {
        $this->callMethodEditStatus = true;
    }
}
